A part of the qbnk/frontend-components package.
This package contains a saml2 authentication adapter which is used to log the user in via an external Identity provider

```php
use Slim\App;
use OneLogin\Saml2\Constants;

$app = new App();
$adapter = new Saml2($app, $settings);
$adapter->authenticate($returnUrl);
```

Register routes for the Slim router

```php
$adapter->registerRoutes($app);
```

Settings sample

```php
$settings = [
    'addQBankAccount' => true,
	'responseUsername' => 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress',
	'responseFirstname' => 'FirstName',
	'responseLastname' => 'LastName',
	'includeUserDataCookie' => true, // For them SPA's
    'jit' => [
        // If enabled, these settings will override the 'addQBankAccount' flag
        'enabled' => true,
        'userType' => 'Frontend',
        'responseGroups' => 'Groups',
        'groupMapping' => [
            'qbankDefaultGroups' => [
                {QBankGroupId1}, {QBankGroupId2}
            ],
            'adGroup1' => [
                {QBankGroupId3}, {QBankGroupId4}
            ],
            'adGroup2' => [
                {QBankGroupId3}, {QBankGroupId5}
            ]
        ]
    ],
	'security' => [
		'wantNameId' => true,
		'nameIdEncrypted' => false,
		'authnRequestsSigned' => false,
		'logoutRequestSigned' => false,
		'logoutResponseSigned' => false,
		'signMetadata' => true,
		'wantMessagesSigned' => true,
		'wantAssertionsSigned' => true,
		'wantNameIdEncrypted' => false,
		'requestedAuthnContext' => false,
		'wantXMLValidation' => true,
	],
	'contactPerson' => [
		'technical' => [
			'givenName' => 'QBNK Tech',
			'emailAddress' => 'tech@qbank.se'
		],
		'support' => [
			'givenName' => 'QBNK Support',
			'emailAddress' => 'support@qbank.se'
		],
	],
	'organization' => [
		'en-US' => [
			'name' => 'QBNK Company',
			'displayname' => 'QBNK Company',
			'url' => 'https://qbank.se/'
		],
	],
	'strict' => false,
	'debug' => true,
	'sp' => [
		'entityId' => '**FILLED IN AUTOMATICALLY**',
		'assertionConsumerService' => [
			'url' => '**FILLED IN AUTOMATICALLY**',
			'binding' => Constants::BINDING_HTTP_POST,
		],
		'singleLogoutService' => [
			'url' => '**FILLED IN AUTOMATICALLY**',
			'binding' => Constants::BINDING_HTTP_REDIRECT,
		],
		'NameIDFormat' => Constants::NAMEID_UNSPECIFIED
	],
	'idp' => [
		'entityId' => '{url}',
		'singleSignOnService' => [
			'url' => '{url}',
			'binding' => Constants::BINDING_HTTP_POST
		],
		'x509cert' => '{base64}'
	]
]

```
